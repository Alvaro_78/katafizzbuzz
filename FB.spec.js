describe("fizzbuzz", () => {

  xit("is a list numbers from 1 to 100 ", () => {

    const fizzbuzz = new FizzBuzz()
    const result = fizzbuzz.listNumbers()
    expect(result.length).toEqual(101)
    expect(result[0]).toEqual(1)
    expect(result[99]).toEqual(100)

  })


  it("is  the number divisible by 3, write Fizz", () => {

    // arrange (ordenar)
    const fizzbuzz = new FizzBuzz()
    // act (actuar)
    const result = fizzbuzz.multipleByThree(3)

    // assert(comprobar)

    expect(result).toBe("Fizz")
  })

  it("is the number divisible by 5, write Buzz", () => {

    const fizzbuzz = new FizzBuzz()

    const result = fizzbuzz.multipleByFive(5)

    expect(result).toBe("Buzz")

  })

  it("is the number divisible by multiple 15 ", () => {

    const fizzbuzz = new FizzBuzz()

    const result = fizzbuzz.multipleByFifteen(15)

    expect(result).toBe("FizzBuzz")

  })

  it("If the number is not divisible neither by 3 nor 5, write the string representation of the number.", () => {

    const fizzbuzz = new FizzBuzz()

    const result = fizzbuzz.multiple(4)

    expect(result).toBe(4)

  })

})



class FizzBuzz {

  multiple(number) {

    const isDivisibleByFifteen = number % 15 === 0
    const isDivisibleByThree = number % 3 === 0
    const isDivisibleByFive = number % 5 === 0
    const isNotMultiple = number

    if (isDivisibleByFifteen) {

      return "FizzBuzz"
    }
    if(isDivisibleByFive) {
      
      return "Buzz"
    }
    if(isDivisibleByThree) {
      
      return "Fizz"
    }
    return number
  }

  multipleByThree(number) {

    const isDivisibleByThree = number % 3 === 0

    if (isDivisibleByThree) {

      return "Fizz"
    }
  }

  multipleByFive(number) {

    const isDivisibleByFive = number % 5 === 0

    if (isDivisibleByFive) {

      return "Buzz"
    }

  }

  multipleByFifteen(number) {

    const isDivisibleByFifteen = number % 15 === 0

    if (isDivisibleByFifteen) {

      return "FizzBuzz"
    }

  }



}
    // listNumbers(){

    //   const array = []
    //   for (let i = 1; i <= 101; i++) {

    //     array.push(i)

    //   }

    //   return array

    // }
